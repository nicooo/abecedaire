extends VBoxContainer


var current_letter = null
var paused_at = null

func get_current_letter():
	return current_letter if current_letter != null else 'A'

func start(letter):
	if Files.get_audio(letter) == null:
		return

	current_letter = letter

	# on charge le fichier audio correspondant à la lettre du bouton
	var sfx = load(Files.get_audio(letter))
	sfx.set_loop(false)

	# on joue l'audio dans le player
	$"music".stream = sfx
	$"music".play()
	exit_pause(false)

	toggle(true, letter)
	playing()

func stop():
	$"music".stop()
	toggle(false)

func toggle(v: bool, letter = null):
	visible = v
	$"/root/world/buttons".visible = not v

	if letter:
		$"/root/world/title".text = Files.get_audio(letter).replace("res://music/", "")
	else:
		$"/root/world/title".text = "Abécédaire musical"

var current_image = null
var dragging = false
func playing():
	if current_letter == null || paused_at != null:
		return

	var curr_pos = $"music".get_playback_position()
	var length = $"music".stream.get_length()

	if not dragging:
		$"slider".value = 100 * curr_pos / length

	# On cherche l'image qu'on doit afficher
	# en fonction de leur nombre et de la durée du morceau en cours
	var images = Files.get_images(current_letter)
	if images.is_empty():
		$"image".texture = null
		return

	var image_index = 0
	for i in range(0, len(images)):
		if curr_pos > floor(length / len(images) * i):
			image_index = i

	var image = images[image_index]
	if image != current_image:
		# On charge l'image en question
		$"image".texture = load(image)
		current_image = image

func _process(_delta):
	playing()

var dragging_value_start = null
func _on_slider_drag_started():
	dragging = true
	_on_slider_value_changed(dragging_value_start)

func _on_slider_drag_ended(value_changed):
	dragging = false

func _on_slider_value_changed(value):
	dragging_value_start = value
	if dragging:
		$"music".seek(value * $"music".stream.get_length() / 100)

func _on_pause_pressed():
	enter_pause() if $"music".playing else exit_pause()

func enter_pause():
	paused_at = $"music".get_playback_position()
	$"music".stop()
	$"/root/world/controls/pause".text = "▶"

func exit_pause(resume_song = true):
	$"music".play()
	if resume_song && paused_at != null:
		$music.seek(paused_at)
	paused_at = null
	$"/root/world/controls/pause".text = "||"
