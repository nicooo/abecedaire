extends VBoxContainer


# On précharge la scène 'button', pour pouvoir la dupliquer 26 fois - de A à Z
var BUTTON = preload("res://button.tscn")

func _ready():
	for letter in Files.letters():
		# on crée un nouveau bouton et on lui attribue la lettre
		var button = BUTTON.instantiate()
		button.text = letter

		# on ajoute le bouton à la grille principale
		find_child("buttons").add_child(button)

func _on_prev_pressed():
	change(-1)

func _on_next_pressed():
	change(+1)

func change(plusminus):
	var current_button = $"slideshow".get_current_letter().unicode_at(0) - 65 + plusminus
	current_button = max(min(current_button, 25), 0)  # bloquer à 0 et 25
	find_child("buttons").get_children()[current_button].emit_signal("pressed")

func _on_stop_pressed():
	$"slideshow".stop()

func _on_music_finished():
	change(+1)
