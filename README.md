# Abécédaire

## Installation

### Télécharger Godot

Ici : https://godotengine.org/download/

### Récupérer le code

Ici : https://framagit.org/nicooo/abecedaire
(bouton téléchargement sur la droite, à côté de *Clone*)

Dézippez le fichier.

### Fichiers musicaux

Copiez vos fichiers musicaux dans le sous-répertoire `music`.

Renommez chaque fichier en le préfixant par la lettre à laquelle il correspond.
Par exemple :

```
A - mon fichier.mp3
B - un autre fichier.mp3
```

### Fichiers images

Copiez vos images dans les sous-répertoires `images/A`, `images/B`, etc.

Par exemple :

```
Dossier "A"
	- fichier "image.png"
	- fichier "image2.jpeg"
Dossier "B"
	- fichier "img.gif"
	- fichier "escargot.jpg"
```

Les images dans un sous-dossier seront affichées pour le diaporama de cette lettre.

Par exemple, si le morceau `A` dure 20 secondes, l'image `image.png` sera affichée pendant les 10 premières secondes de lecture, puis l'image `image2.jpeg` pendant 10 secondes également.

### Lancer l'abécédaire

Ouvrez Godot et dans le *Gestionnaire de projets* qui s'affiche :

1. Cliquez sur *Importer* ;
2. Cliquez sur *Parcourir* et dans le répertoire principal (`abécédaire`), sélectionnez le fichier `project.godot` ;
3. Cliquez sur *Importer et modifier* ;
4. Appuyez sur **F5** pour lancer l'abécédaire.
