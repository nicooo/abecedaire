extends Node


var audio_files = {}
var images = {}

func _ready():
	load_audio_files()
	load_images()

func get_audio(letter):
	return audio_files[letter] if letter in audio_files else null

func get_images(letter):
	return images[letter]

# Renvoie le tableau des lettres de l'alphabet ["A", "B", ..., "Z"]
func letters():
	var letters = []
	for l in range(0, 26):
		letters.append(char(65 + l))  # 65: A, 66: B, 67: C…
	return letters

func load_audio_files():
	var musicpath = "res://music/"
	var dir = DirAccess.open(musicpath)

	if not dir:
		return

	for file in dir.get_files():
		if file.ends_with(".import"):  # ne pas lire les fichiers ".import" de Godot
			continue

		for letter in letters():
			if file.begins_with(letter):
				audio_files[letter] = musicpath + file

func load_images():
	for letter in letters():
		images[letter] = []

		var dirpath = "res://images/" + letter + "/"
		var dir = DirAccess.open(dirpath)
		if not dir:
			continue

		for file in dir.get_files():
			if file.ends_with(".import"):  # ne pas lire les fichiers ".import" de Godot
				continue
			images[letter].append(dirpath + file)
