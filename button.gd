extends Button


var COLORS = ["#2ecc71", "#f39c12", "#e74c3c", "#9b59b6", "#3498db"]

var images = []


func _ready():
	# Si on n'a pas de fichier audio défini pour une lettre, on grise le bouton
	if Files.get_audio(text) == null:
		disabled = true
		return

	images = Files.get_images(text)

	# Choisir une couleur tournante en fonction de la lettre du bouton
	var l = text.unicode_at(0) - 65
	for color in ["font_color", "font_pressed_color", "font_focus_color"]:
		self["theme_override_colors/" + color] = Color(COLORS[l % len(COLORS)])

func _on_pressed(): # au clic sur le bouton
	# On démarre le diaporama d'images
	$"/root/world/slideshow".start(text)
